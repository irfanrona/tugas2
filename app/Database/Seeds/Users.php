<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class Users extends Seeder
{
    public function run()
    {
        // membuat data
		$users_data = [
			[
				'role_id' => 1, //Admin
				'fullname' => 'Jhon Doe',
                'phone' => '08123456789',
                'email' => 'jhondoe@indoapril.com',
                'age' => 36,
                'username' => 'jhon',
				'password'  => 'jhon123' //belum di enkripsi
			],
			[
				'role_id' => 2, //Head Department
				'fullname' => 'Keanu Doe',
                'phone' => '08123456788',
                'email' => 'keanudoe@indoapril.com',
                'age' => 32,
                'username' => 'keanu',
				'password'  => 'keanu123' //belum di enkripsi
			],
			[
				'role_id' => 3, //Employee
				'fullname' => 'Rona Doe',
                'phone' => '08123456787',
                'email' => 'ronadoe@indoapril.com',
                'age' => 28,
                'username' => 'rona',
				'password'  => 'rona123' //belum di enkripsi
			]
		];

		foreach($users_data as $data){
			// insert semua data ke tabel
			$this->db->table('users')->insert($data);
		}
    }
}
