<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class Roles extends Seeder
{
    public function run()
    {
        // membuat data
		$roles_data = [
			[
				'name' => 'Admin',
				'description'  => 'ini adalah role paling tinggi, dia dapat melakukan akses ke setiap fitur yang ada'
			],
			[
				'name' => 'Head Department',
				'description'  => 'ini adalah role yang dapat melakukan akses ke setiap fitur yang ada tetapi hanya di department nya saja'
			],
			[
				'name' => 'Employee',
				'description'  => 'ini adalah role paling rendah yang hanya diberikan fitur terbatas'
			]
		];

		foreach($roles_data as $data){
			// insert semua data ke tabel
			$this->db->table('roles')->insert($data);
		}
    }
}
