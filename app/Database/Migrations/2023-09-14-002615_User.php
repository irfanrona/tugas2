<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class User extends Migration
{
    public function up()
    {
        $this->forge->addField([
			'id' => [
				'type'           => 'INT',
				'constraint'     => 11,
				'unsigned'       => true,
				'auto_increment' => true
			],
            'role_id' => [
				'type'           => 'INT',
				'constraint'     => 11,
				'unsigned'       => true,
			],
			'fullname'  => [
				'type'           => 'VARCHAR',
				'constraint'     => '100'
			],
            'phone'  => [
				'type'           => 'VARCHAR',
				'constraint'     => '16'
			],
            'email' => [
				'type'           => 'VARCHAR',
				'constraint'     => '100'
			],
            'age' => [
				'type'           => 'INT',
				'constraint'     => 11,
				'unsigned'       => true,
			],
            'username'  => [
				'type'           => 'VARCHAR',
				'constraint'     => '255'
			],
            'password'  => [
				'type'           => 'VARCHAR',
				'constraint'     => '255'
			],
		]);

		$this->forge->addKey('id', TRUE);

		$this->forge->createTable('users', TRUE);
    }

    public function down()
    {
        $this->forge->dropTable('users');
    }
}
