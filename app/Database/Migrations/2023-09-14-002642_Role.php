<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Role extends Migration
{
    public function up()
    {
        $this->forge->addField([
			'id' => [
				'type'           => 'INT',
				'constraint'     => 11,
				'unsigned'       => true,
				'auto_increment' => true
			],
            'name' => [
				'type'           => 'VARCHAR',
				'constraint'     => '255'
			],
            'description' => [
				'type'           => 'TEXT',
				'null'           => true
			],
		]);

		$this->forge->addKey('id', TRUE);

		$this->forge->createTable('roles', TRUE);
    }

    public function down()
    {
        $this->forge->dropTable('roles');
    }
}
