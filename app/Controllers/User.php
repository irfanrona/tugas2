<?php

namespace App\Controllers;
use App\Models\UserModel;
use App\Models\RoleModel;

class User extends BaseController
{
    public function index(): string
    {
        $userModel = new UserModel();
        $users = $userModel->findAll();

        $data['users'] = $users;
        
        return view('v_user',$data);
    }
}
